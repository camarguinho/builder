package auto.estudo;


public class TesteDaNotaFiscal {

	public static void main(String[] args) {
		
		NotaFiscalBuilder nfBuilder = new NotaFiscalBuilder();
		nfBuilder.comCnpj("123").
		comObservacao("mais um teste").
		comItem(new ItemNota("item 1", 10));
		NotaFiscal nf = nfBuilder.constroi();
		
		System.out.println(nf.observacoes);
	}
	
}
